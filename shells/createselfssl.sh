#!/bin/bash

openssl req -subj '/CN=localhost' -x509 -newkey rsa:4096 -nodes -keyout ./nginx/cert/key.pem -out ./nginx/cert/cert.pem -days 365
