#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Please provide an username"
    exit 0
fi

htpasswd -c ./nginx/pass/.htpasswd $1
