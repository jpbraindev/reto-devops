###############################################################
#                                                             #
#            TODO                                             #
#                                                             #
###############################################################

# Variables setup

REPONAME  := jpbraindev/clm-reto-devops
AUTHFILE  := ./nginx/pass/.htpasswd
CERTFILE   := ./nginx/cert/cert.pem
KEYFILE   := ./nginx/cert/key.pem
USERADMIN := admin

.PHONY: installbase image adminuser composerun composestop certificado clean default runtest all


default:
	echo "######################################################"
	echo "#                                                    #"
	echo "# Use one of the following options:                  #"
	echo "#                                                    #"
	echo "#     installbase, image, adminuser                  #" 
	echo "#     composerun, composestop, certificado, clean    #" 
	echo "#     runtest                                        #"
	echo "#                                                    #"
	echo "######################################################"
all:

# dependencies and base
installbase: clean
	sudo apt-get install node npm apache2-utils
	npm install

#create docker image  (reto 1)
image: Dockerfile
	docker build . -t $(REPONAME)

#create admin user for basic auth
adminuser: 
	htpasswd -c $(AUTHFILE) $(USERADMIN)

# run docker compose (reto 2)
composerun: docker-compose.yml image certificado
	docker-compose up -d

# stop and remove services in docker compose (reto 2b)
composestop: docker-compose.yml
	docker-compose down

# create self signed certificate (reto 2c)
certificado: 
	openssl req -subj '/CN=localhost' -x509 -newkey rsa:4096 -nodes -keyout $(KEYFILE) -out $(CERTFILE) -days 365
       
runtest:
	npm run test
       
clean: 
	rm -f package-lock.json
