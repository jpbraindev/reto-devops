#Eleccion container base
FROM node:17-slim

#dicrectorio destino
WORKDIR /usr/src/reto-devops
COPY package*.json ./

#Instalar dependencias
RUN npm install

#copiar la aplicacion
copy . . 

# exponer el puerto externo
expose 3000

# inicio aplicacion
CMD ["node", "index.js" ]

# Health Check (para portainer)
HEALTHCHECK CMD ["node", "./tests/health.js" ] || exit 1 
