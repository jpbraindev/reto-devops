# Reto-DevOps CLM

### Pre-Instalacion
- El reto fue realizado en un servidor Ubuntu 20.04 con docker.
- Ejecutar "make installbase" para la installacion del software necesario


### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

- Archivo "Dockerfile" en base la imagen node17:slim
- Cree un archivo test/health.js para incluir un health check en el dockerifle 
- Ejecutar "make image" para crear la imagen en docker.

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

- Archivo "docker-compose.yml" en base a imgen nginx y la imagen creada en el reto 1
- Ejecutar "make composerun" (correra las dependencias necesarias)


### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

- Se creo un pipeline basico de instalacion y prueba para la aplicacion. 

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

- No realizado

### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

- No realizado

### Reto 6. Terraform
![docker](./img/tf.png)

- No realizado

### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

- archivo Makefile creado. Se fue creando a medida que se completaban los retos.
- adicionalmente a shells para cada paso en el subdirectorio shells.
